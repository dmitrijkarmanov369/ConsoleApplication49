﻿#include <iostream>
#include <ctime>
using namespace std;

typedef unsigned int uint;

void perfectNumber(uint& a, uint& b);

int main()
{
	setlocale(LC_ALL, "ru");

	uint a;
	uint b;

	cout << "Поиск совершенных чисел во введенном интервале" << endl << endl;
	cout << "Введите первое число: "; 
	cin >> a; 
	cout << endl;
	cout << "Введите второе число: "; 
	cin >> b; 
	cout << endl;


	cout << "Совершенные числа во введеном интервале: | ";
	perfectNumber(a, b);

	cout << endl;
	return 0;
}

void perfectNumber(uint& a, uint& b) {

	for (uint i = a; i <= b; i++)
	{
		uint sum = 0;

		for (uint j = 1; j < i; j++) {
			if (i % j == 0)
				sum += j;
		}

		if (sum == i) {
			cout << sum << " | ";
		}
	}
}